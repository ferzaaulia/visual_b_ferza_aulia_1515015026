
package Posttest2;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class posttest22 extends javax.swing.JFrame {

    
    public posttest22() {
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        name = new javax.swing.JLabel();
        Nm = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        notlp = new javax.swing.JTextField();
        BrtBrg = new javax.swing.JLabel();
        bbkg = new javax.swing.JTextField();
        kg = new javax.swing.JLabel();
        kota = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        diskon = new javax.swing.JSlider();
        tx_slider = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        reg = new javax.swing.JRadioButton();
        expres = new javax.swing.JRadioButton();
        jLabel8 = new javax.swing.JLabel();
        totalhrg = new javax.swing.JTextField();
        proses = new javax.swing.JButton();
        ulng = new javax.swing.JButton();
        keluar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Pelanggan");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLocationByPlatform(true);
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 0));

        name.setText("Nama");

        Nm.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                NmKeyReleased(evt);
            }
        });

        jLabel2.setText("No. Telpon");

        notlp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                notlpKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                notlpKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                notlpKeyTyped(evt);
            }
        });

        BrtBrg.setText("Berat Barang");

        bbkg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bbkgActionPerformed(evt);
            }
        });
        bbkg.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bbkgKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bbkgKeyReleased(evt);
            }
        });

        kg.setText("KG");

        kota.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Kota 1", "Kota 2", "Kota 3", "Kota 4" }));

        jLabel9.setText("Pilih Kota");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Nm)
                    .addComponent(notlp)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(name)
                            .addComponent(jLabel2)
                            .addComponent(BrtBrg)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(bbkg, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(kg))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(kota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 63, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(name)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Nm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(notlp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BrtBrg)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bbkg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(kg))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 0));

        diskon.setValue(0);
        diskon.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                diskonMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                diskonMouseMoved(evt);
            }
        });
        diskon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                diskonMouseReleased(evt);
            }
        });

        tx_slider.setText("0%");

        jLabel6.setText("Atur Diskon");

        jLabel7.setText("Jenis Jasa");

        buttonGroup1.add(reg);
        reg.setText("Reguler");
        reg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                regMouseReleased(evt);
            }
        });

        buttonGroup1.add(expres);
        expres.setText("Express");
        expres.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                expresMouseReleased(evt);
            }
        });

        jLabel8.setText("Total Harga");

        totalhrg.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(reg)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(expres))
                            .addComponent(jLabel8))
                        .addGap(0, 51, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(diskon, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tx_slider))
                    .addComponent(totalhrg))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tx_slider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(diskon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(reg)
                    .addComponent(expres))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(totalhrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        proses.setText("Proses");
        proses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prosesActionPerformed(evt);
            }
        });

        ulng.setText("Ulangi");
        ulng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ulngActionPerformed(evt);
            }
        });

        keluar.setText("Keluar");
        keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                keluarActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 0));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Form Pelanggan Asongan");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(106, 106, 106)
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel5)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ulng)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(proses)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(keluar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(keluar)
                    .addComponent(ulng)
                    .addComponent(proses))
                .addContainerGap(36, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleDescription("");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void diskonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_diskonMouseReleased
       
    }//GEN-LAST:event_diskonMouseReleased

    private void diskonMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_diskonMouseMoved
        
    }//GEN-LAST:event_diskonMouseMoved

    private void diskonMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_diskonMouseDragged
       
                String valueOf = String.valueOf(diskon.getValue());
                tx_slider.setText(valueOf+"%");      
    }//GEN-LAST:event_diskonMouseDragged

    private void keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_keluarActionPerformed
    
        System.exit(0);
    }//GEN-LAST:event_keluarActionPerformed

    private void ulngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ulngActionPerformed
        
        
        Nm.setText("");
        notlp.setText("");
        totalhrg.setText("");
        tx_slider.setText("0%");
        diskon.setValue(0);
        buttonGroup1.clearSelection();
        proses.setEnabled(false);
        
    }//GEN-LAST:event_ulngActionPerformed

    private void prosesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prosesActionPerformed
        
        double express = 0, diskon = 0,reguler = 0, total_harga=0;
        if(Nm.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Nama Tidak Boleh Kosong");        
        }else if(notlp.getText().equals("")){
            JOptionPane.showMessageDialog(null, "No. Telp Tidak Boleh Kosong");        
        }else if(bbkg.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Berat Barang Tidak Boleh Kosong");
        }else if(!expres.isSelected() && !reg.isSelected()){
            JOptionPane.showMessageDialog(null, "Jenis Jasa Belum Dipilih");
        }else{
          diskon = diskon.getValue()/100;
          if(expres.isSelected()){
              express = Integer.parseInt(bbkg.getText())*11500;  
              total_harga = express - (diskon*express);
          }else{
              reguler = (11500*0.5)*Integer.parseInt(bbkg.getText());
              total_harga = reguler - (diskon*reguler);
          }
          totalhrg.setText(String.valueOf(total_harga));
        } 
    }//GEN-LAST:event_prosesActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
            
    }//GEN-LAST:event_formWindowClosed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        // TODO add your handling code here:
        proses.setEnabled(false);
    }//GEN-LAST:event_formWindowActivated

    private void NmKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NmKeyReleased
        // TODO add your handling code here:
        if(Nm.getText().equals("")||notlp.getText().equals("")||bbkg.getText().equals("")||(!expres.isSelected() && !reg.isSelected())){
            proses.setEnabled(false);
        }else {
            proses.setEnabled(true);
        }
    }//GEN-LAST:event_NmKeyReleased

    private void notlpKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_notlpKeyReleased
        // TODO add your handling code here:
        if(Nm.getText().equals("")||notlp.getText().equals("")||bbkg.getText().equals("")||(!expres.isSelected() && !reg.isSelected())){
            proses.setEnabled(false);
        }else {
            proses.setEnabled(true);
        }
    }//GEN-LAST:event_notlpKeyReleased

    private void regMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_regMouseReleased
        // TODO add your handling code here:
        if(Nm.getText().equals("")||notlp.getText().equals("")||bbkg.getText().equals("")||(!expres.isSelected() && !reg.isSelected())){
            proses.setEnabled(false);
        }else {
            proses.setEnabled(true);
        }
    }//GEN-LAST:event_regMouseReleased

    private void notlpKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_notlpKeyTyped
        // TODO add your handling code here:
        
    }//GEN-LAST:event_notlpKeyTyped

    private void notlpKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_notlpKeyPressed
        // TODO add your handling code here:
        JTextField textField = new JTextField(10);
        notlp.addKeyListener(new KeyAdapter() {
           public void keyTyped(KeyEvent e) {
              char c = e.getKeyChar();
              if ( ((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE)) {
                 e.consume();  // ignore event
              }
           }
        });
    }//GEN-LAST:event_notlpKeyPressed

    private void bbkgKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bbkgKeyReleased
        // TODO add your handling code here:
        if(Nm.getText().equals("")||notlp.getText().equals("")||bbkg.getText().equals("")||(!expres.isSelected() && !reg.isSelected())){
            proses.setEnabled(false);
        }else {
            proses.setEnabled(true);
        }
    }//GEN-LAST:event_bbkgKeyReleased

    private void bbkgKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bbkgKeyPressed
        // TODO add your handling code here:
        JTextField textField = new JTextField(10);
        bbkg.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if ( ((c < '0') || (c > '9') ) && (c != KeyEvent.VK_BACK_SPACE) && (c != KeyEvent.VK_PERIOD)) {
                    e.consume();  // ignore event
                }
            }
        });
    }//GEN-LAST:event_bbkgKeyPressed

    private void bbkgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bbkgActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bbkgActionPerformed

    private void expresMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_expresMouseReleased
        // TODO add your handling code here:
        if(Nm.getText().equals("")||notlp.getText().equals("")||bbkg.getText().equals("")||(!expres.isSelected() && !reg.isSelected())){
            proses.setEnabled(false);
        }else {
            proses.setEnabled(true);
        }
    }//GEN-LAST:event_expresMouseReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(posttest22.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(posttest22.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(posttest22.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(posttest22.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new posttest22().setVisible(true);
            
            }
            
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BrtBrg;
    private javax.swing.JTextField Nm;
    private javax.swing.JTextField bbkg;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JSlider diskon;
    private javax.swing.JRadioButton expres;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton keluar;
    private javax.swing.JLabel kg;
    private javax.swing.JComboBox kota;
    private javax.swing.JLabel name;
    private javax.swing.JTextField notlp;
    private javax.swing.JButton proses;
    private javax.swing.JRadioButton reg;
    private javax.swing.JTextField totalhrg;
    private javax.swing.JLabel tx_slider;
    private javax.swing.JButton ulng;
    // End of variables declaration//GEN-END:variables

}
